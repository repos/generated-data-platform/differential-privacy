# system packages
import argparse
from math import sqrt, log as ln
import getpass

# local metric functions
from differential_privacy.utils.metrics import calc_error_country_project_page
from differential_privacy.utils.budget import write_to_global_privacy_budget

# pyspark
from pyspark.sql import SparkSession
from pyspark.sql import functions as sf
import pyspark.sql.types as spty

# tumult analytics
from tmlt.analytics.privacy_budget import RhoZCDPBudget
from tmlt.analytics.query_builder import QueryBuilder, ColumnType
from tmlt.analytics.session import Session
from tmlt.analytics.keyset import KeySet

# tumult core
from tmlt.core.domains.spark_domains import SparkDataFrameDomain
from tmlt.core.utils.truncation import truncate_large_groups
from tmlt.core.utils.cleanup import cleanup

#------------ START MAIN SCRIPT ------------#
EPSILON = 1
DELTA = 1e-7
PV_THRESH = 150
CONTRIB_THRESH = 10
RELEASE_THRESH = 90

# all pageviews for a day from pageview_actor
pv_query = """
SELECT
  page_id,
  pageview_info['project'] as project,
  geocoded_data['country'] as country,
  actor_signature
FROM wmf.pageview_actor
WHERE
  is_pageview
  AND namespace_id = 0
  AND year = {year}
  AND month = {month}
  AND day = {day}
"""

# select all unique pages from pageview_actor with more than `count` views
filter_query = """
SELECT
  pageview_info['project'] AS project,
  page_id
FROM
  wmf.pageview_actor
WHERE
  is_pageview
  AND COALESCE(pageview_info['project'], '') != ''
  AND year = {year}
  AND month = {month}
  AND day = {day}
GROUP BY 1, 2
HAVING count(*) >= {pv_thresh}
"""

# all countries we are releasing data for
country_query = """
SELECT DISTINCT
  country_name as country
FROM
  wmf.geoeditors_public_monthly
WHERE country_code != '--'
"""

# all countries, subcontinents, and continents for metrics
geo_metrics_query = """
SELECT
    country,
    subcont_region as subcontinent,
    continent
FROM
    isaacj.country_to_region
"""

def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description='Specify context for differential privacy aggregation of pageview_actor'
    )
    parser.add_argument('year', help='year of aggregation')
    parser.add_argument('month', help='month of aggregation')
    parser.add_argument('day', help='day of aggregation')
    parser.add_argument('write_db', help='''
        table to write to
        "default" -> `differential_privacy.country_language_page_eps_X_delta_Y`
        "user" -> `<username>.country_language_page_eps_X_delta_Y
        any other string -> <string>.country_language_page_eps_X_delta_Y`
    ''')
    return parser.parse_args()

def gen_rho(epsilon, delta):
    logterm = ln(1 / delta)
    # Theorem 3.5 in https://arxiv.org/pdf/1605.02065.pdf
    # This is not tight, but should be a good enough approximation for experimentation
    return (sqrt(epsilon+logterm) - sqrt(logterm))**2

def dp_aggregate_flatmap(spark, df, key_df, rho, contrib_thresh, log):
    log.info("generating keyset...")
    # We convert the <page_id, project, country> triple into a single ID string
    # by putting null characters between the three; we will later group on this
    # single ID
    flatmap_key_rdd = (
        key_df.rdd.map(lambda r: (f'{r[0]}\0{r[1]}\0{r[2]}', ))
    )
    flatmap_key_schema = spty.StructType([
        spty.StructField('id', spty.StringType(), False)
    ])
    flatmap_key_df = spark.createDataFrame(
        flatmap_key_rdd, flatmap_key_schema
    )
    flatmap_key_df.cache()
    flatmap_key_df.take(1)
    flatmap_ks = KeySet.from_dataframe(flatmap_key_df)

    log.info("preprocessing data...")
    # We take all the rows associated with a single actor_signature, and merge
    # the page_ids, projects, and countries associated with this actor_signature
    # using the same insert-null-characters trick as above.
    reduced_rdd = (
        df.select("*").distinct().rdd
        .map(lambda r: (r[3], [(r[0],r[1],r[2])]))
        .reduceByKey(lambda x,y: x+y)
        .map(lambda r: (
                r[0],
                '\0'.join(str(pid[0]) for pid in r[1]),
                '\0'.join(str(pid[1]) for pid in r[1]),
                '\0'.join(str(pid[2]) for pid in r[1]),
             )
        )
    )
    reduced_rdd_schema = spty.StructType([
        spty.StructField('actor_signature', spty.StringType(), False),
        spty.StructField('page_ids', spty.StringType(), False),
        spty.StructField('projects', spty.StringType(), False),
        spty.StructField('countries', spty.StringType(), False)
    ])
    df_reduced = spark.createDataFrame(reduced_rdd, reduced_rdd_schema)
    df_reduced.cache()
    df_reduced.take(1)

    log.info("establishing session...")
    session = Session.from_dataframe(
        privacy_budget=RhoZCDPBudget(float('inf')),
        source_id="reduced_pageview_actor",
        dataframe=df_reduced
    )

    # This takes the rows in which page_ids, projects and countries have been
    # squashed into the corresponding columns, un-aggregates them, and
    # associates each row to the ID matching the KeySet created above.
    def split_reduced_row(r):
        page_ids = r['page_ids'].split('\0')
        projects = r['projects'].split('\0')
        countries = r['countries'].split('\0')
        n_pvs = len(page_ids)
        if len(projects) != n_pvs or len(countries) != n_pvs:
            raise RuntimeError('mismatched lengths')
        return [
            {
                'id': f'{countries[i]}\0{projects[i]}\0{page_ids[i]}'
            }
            for i in range(n_pvs)
        ]

    # This applies the function above, using the grouping=True option to
    # indicate to Tumult Analytics that the new column created by the flatmap
    # will be used as a groupby key afterwards, allowing good privacy accounting.
    # After the flatmap, it performs the actual groupby + count operation.
    log.info("running query...")
    query = (
        QueryBuilder("reduced_pageview_actor")
        .flat_map(
            split_reduced_row,
            max_num_rows=contrib_thresh,
            new_column_types={
                'id': ColumnType.VARCHAR,
            },
            augment=True,
            grouping=True
        )
        .groupby(flatmap_ks)
        .count(name='gbc')
    )
    private = session.evaluate(query, RhoZCDPBudget(rho))

    log.info("postprocessing...")

    def split_fields(r):
        country, project, page_id = r[0].split('\0')
        return (country, project, int(page_id), r[1])

    private_split_rdd = private.rdd.map(split_fields)
    private_split_schema = spty.StructType([
        spty.StructField("country", spty.StringType(), False),
        spty.StructField("project", spty.StringType(), False),
        spty.StructField("page_id", spty.IntegerType(), False),
        spty.StructField("gbc", spty.IntegerType(), False),
    ])
    private_split = spark.createDataFrame(private_split_rdd, private_split_schema).cache()
    return private_split

def get_nonprivate(df, key_df, contrib_thresh):
    ks = KeySet.from_dataframe(key_df)
    df_limited = truncate_large_groups(
        df.select("*").distinct(),
        ["actor_signature"],
        contrib_thresh
    )
    return df_limited.groupby('country', 'project', 'page_id').count()

def run_dp(args, spark, log):
    if args.write_db == 'default':
        database = 'differential_privacy'
    elif args.write_db == 'user':
        database = getpass.getuser()
    else:
        database = args.write_db

    output_table = (
        f"{database}.country_language_page_eps_{str(EPSILON).replace('.', '_')}"
        f"_delta_{str(DELTA).replace('-', '_')}"
        f"_{args.year}_{args.month}_{args.day}"
    )
    log.info(f"output table: {output_table}")

    log.info("querying hive tables...")
    # select all pageviews from a day and drop null values
    df = spark.sql(pv_query.format(year=args.year, month=args.month, day=args.day))
    df = df.dropna()

    # select all countries we're releasing data for
    country_df = spark.sql(country_query)
    country_df.cache()
    country_df.take(1)

    # get geo regions for metrics
    geo_metrics_df = spark.sql(geo_metrics_query)

    # create Tumult df domain
    domain = SparkDataFrameDomain.from_spark_schema(df.schema)

    # create article
    article_df = spark.sql(
        filter_query.format(year=args.year,
                            month=args.month,
                            day=args.day,
                            pv_thresh=PV_THRESH)
    )
    article_df.cache()
    article_df.take(1)

    log.info("creating keyspace")
    # cross join countries and articles to get keyspace and cache
    key_df = country_df.crossJoin(article_df)
    key_df = key_df.dropna()
    key_df.take(1)

    log.info("running private aggregation")
    private = dp_aggregate_flatmap(
        spark=spark,
        df=df,
        key_df=key_df,
        rho=gen_rho(epsilon=EPSILON, delta=DELTA),
        contrib_thresh=CONTRIB_THRESH,
        log=log
    )

    log.info('running nonprivate aggregation...')
    nonprivate = get_nonprivate(
        df=df,
        key_df=key_df,
        contrib_thresh=CONTRIB_THRESH
    )

    log.info('joining tables for error calculations...')
    private_rounded = (
        private.withColumn("gbc", sf.when(sf.col("gbc") < 0, 0).otherwise(sf.col("gbc")))
    )
    private_rounded_geo = private_rounded.join(geo_metrics_df, on=['country'])
    joined = (
        nonprivate.join(private_rounded_geo, ['country', 'project', 'page_id'], how='outer')
        .na.fill({'count': 0, 'gbc': 0})
    )

    log.info('conducting error calculations...')
    calc_error_country_project_page(
        df=joined,
        spark=spark,
        year=args.year,
        month=args.month,
        day=args.day
    )

    log.info(f"filtering output to only counts > {RELEASE_THRESH}...")
    # filter to just entries above threshold (90)
    private = private.filter(f"gbc >= {RELEASE_THRESH}")

    log.info("saving final table...")
    # save output
    private.write.saveAsTable(output_table) # TODO: include option to save to custom file path

    # write to privacy register
    log.info("writing to global privacy register...")
    write_to_global_privacy_budget(
        spark=spark,
        table="wmf.pageview_actor",
        epsilon=EPSILON,
        delta=DELTA,
        rho=gen_rho(epsilon=EPSILON, delta=DELTA),
        year=args.year,
        month=args.month,
        day=args.day
    )
#------------- END MAIN SCRIPT -------------#

def main():
    # parse args
    args = parse_args()

    # get spark session and run main
    spark = SparkSession.builder.getOrCreate()
    # TODO: investigate if there's a better way to do this
    log = spark.sparkContext._jvm.org.apache.log4j.LogManager.getLogger(__name__)

    run_dp(args=args, spark=spark, log=log)

    # cleanup tumult analytics and spark cluster
    log.info("cleaning up tmlt.analytics and stopping spark...")
    cleanup()
    spark.sparkContext.stop()
    spark.stop()
    log.info("done")

if __name__ == "__main__":
    main()
