#!/usr/bin/env python
# coding: utf-8

"""
A pyspark job to prepare data for KeySet computation.

$SPARK_HOME/bin/spark-submit differential_privacy/data_preparation.py \
        --start-date 2022-03-01 \
        --end-date 2022-03-02 \
        --output-path pa_keyset
"""

import os
from datetime import datetime

import configargparse
from pyspark.sql import SparkSession

pv_query = """
SELECT DISTINCT
  pageview_info['page_title'] as page_title,
  page_id,
  pageview_info['project'] as project
  , geocoded_data['country'] as country
  , actor_signature
FROM wmf.pageview_actor
WHERE
  is_pageview
  AND namespace_id = 0
  AND concat_ws('-', year, month, day) BETWEEN '{start_date}' AND '{end_date}'
  AND lower(pageview_info['page_title']) != 'nan'
"""

# select all unique pages from pageview_actor with more than `count` views
filter_query = """
SELECT
  pageview_info['project'] AS project,
  page_id,
  count(*) as page_views
FROM
  wmf.pageview_actor
WHERE
  is_pageview
  AND COALESCE(pageview_info['project'], '') != ''
  AND concat_ws('-', year, month, day) BETWEEN '{start_date}' AND '{end_date}'
GROUP BY 1, 2
HAVING count(*) >= {min_page_views}
"""

# all countries we are releasing data for
country_query = """
SELECT DISTINCT
  country_name as country
FROM
  wmf.geoeditors_public_monthly
WHERE country_code != '--'
"""


def format_dt(dt):
    """
    Strip the leading 0 from the day/month of a ISO 8601 string.

    :param dt: a YYYY-MM-DD string representing a date.
    """
    return datetime.strptime(dt, "%Y-%m-%d").strftime("%Y-%-m-%-d")


def make_argparse():
    parser = configargparse.ArgumentParser(
        description="Prepare data for KeySet computation"
    )
    parser.add(
        "-c", "--config", required=False, is_config_file=True, help="config file path"
    )
    parser.add_argument(
        "--output-path",
        required=True,
        metavar="output_path",
        type=str,
        help="Output path",
        default="pa_key_set-2",
    )
    parser.add_argument(
        "--start-date",
        metavar="start_date",
        type=str,
        help="Start date YYYY-MM-DD",
        default=datetime.today().strftime("%Y-%-m-%-d"),
    )
    parser.add_argument(
        "--end-date",
        metavar="end_date",
        type=str,
        help="End date YYYY-MM-DD",
        default=datetime.today().strftime("%Y-%m-%d"),
    )
    parser.add_argument(
        "--min-page-views",
        metavar="min_page_views",
        type=int,
        help="select all unique pages from pageview_actor with more than `count` views",
        default=10,
    )
    parser.add_argument(
        "--overwrite",
        metavar="overwrite",
        type=bool,
        help="Overwrite data in output_path",
        default=True,
    )
    return parser


def main():
    parser = make_argparse()
    args = parser.parse_args()
 
    spark = SparkSession.builder.getOrCreate()

    start_date = format_dt(args.start_date)
    end_date = format_dt(args.end_date)
    min_page_views = args.min_page_views

    mode = "error"
    if args.overwrite:
        mode = "overwrite"

    page_views_df = spark.sql(pv_query.format(start_date=start_date, end_date=end_date))
    article_df = spark.sql(
        filter_query.format(
            start_date=start_date, end_date=end_date, min_page_views=min_page_views
        )
    )
    country_df = spark.sql(country_query)
    # Cache `country_df` and materialize it before the crossJoin starts.
    country_df.cache()
    country_df.collect()

    key_df = country_df.crossJoin(article_df.select("project", "page_id"))

    # TODO: error handling with a reasonable fallback in case we run
    # with spark2 and/or dynamic allocation.
    num_cores = int(spark.conf.get("spark.executor.cores"))
    num_executors = int(spark.conf.get("spark.executor.instances"))
    key_df.repartition(num_executors * num_cores)

    # TODO: think about basePath and partitioning scheme
    key_df.write.mode(mode).parquet(os.path.join(args.output_path, "keys"))
    article_df.write.mode(mode).parquet(os.path.join(args.output_path, "article"))
    country_df.write.mode(mode).parquet(os.path.join(args.output_path, "country"))
    page_views_df.write.mode(mode).parquet(os.path.join(args.output_path, "page_views"))

    spark.stop()


if __name__ == "__main__":
   main()

